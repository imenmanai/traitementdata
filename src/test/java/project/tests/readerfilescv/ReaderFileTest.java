package project.tests.readerfilescv;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.hadoop.shaded.org.checkerframework.checker.units.qual.C;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import project.classes.Configuration;
import project.functions.readerscvfile.ReaderFile;

import javax.ws.rs.core.Configurable;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ReaderFileTest {
    @Test
    public void readTest()
    {   SparkConf sparkConf;
        SparkSession sparkSession;
        Config config1 = ConfigFactory.load("application.conf");
        sparkConf= new SparkConf().setMaster("local[2]").setAppName("WorkAccidentTest");
        sparkSession= SparkSession.builder().config(sparkConf).getOrCreate();
        String   inputPathStr = config1.getString("3il.path.input");
        List<String> points = Arrays.asList("2018-01-09","17h","Administratif","Lumen Animatrice","Lumen CCRC","AT","","","","","non","non","","Convulsions/ spasmes et PC","Côté droit","Déplacement","Chute ","une marche d'une estrade","");
        Dataset<Row>   data = sparkSession.createDataset(points, Encoders. STRING()).toDF("Accidents");
        ReaderFile tf = new ReaderFile(inputPathStr,sparkSession);
        Dataset<Row> lines = tf.get();
        assertThat(lines.first().getString(0)).isEqualTo(data.first().getString(0));

    }

}
