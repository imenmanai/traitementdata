package project.tests.mapperfilecsv;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.Test;
import project.classes.Configuration;
import project.functions.accidents.MapperFileSCV;
import project.functions.readerscvfile.ReaderFile;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MapperFileCsvTest {
    @Test
    public void mapperfilecsv()
    {   SparkConf sparkConf;
        SparkSession sparkSession;
        sparkConf= new SparkConf().setMaster("local[2]").setAppName("WorkAccidentTest");
        sparkSession= SparkSession.builder().config(sparkConf).getOrCreate();
        List<String> points = Arrays.asList("2018-01-09","17h","Administratif","Lumen Animatrice","Lumen CCRC","AT","","","","","non","non","","Convulsions/ spasmes et PC","Côté droit","Déplacement","Chute ","une marche d'une estrade","");
        Dataset<Row> data = sparkSession.createDataset(points, Encoders. STRING()).toDF("Accidents");
        Configuration conf = new Configuration();
        ReaderFile tf = new ReaderFile(conf.getInputPathStr(), conf.getSparkSession());
        MapperFileSCV m = new MapperFileSCV();
        MapperFileSCV m2 = new MapperFileSCV();

        assertThat(m2.apply(data)).isEqualTo( m.apply(tf.get()));




    }
}
