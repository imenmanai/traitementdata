package project.tests.writefilecsv;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import project.functions.writerfile.ResultWriter;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
public class WritefileTest {
    SparkSession sparkSession = SparkSession.builder().master("local[2]").appName("tst-writer")
            .getOrCreate();
    String outputFolderPathStr = "target/output/monfichier.txt";

    @Before
    public void setup(){
        cleanup();
    }
    @Test
    public void WriterfileTest()
    {
        ResultWriter writerCollections = new ResultWriter(outputFolderPathStr);
        List<String> points = Arrays.asList("2018-01-09","17h","Administratif","Lumen Animatrice","Lumen CCRC","AT","","","","","non","non","","Convulsions/ spasmes et PC","Côté droit","Déplacement","Chute ","une marche d'une estrade","");
        Dataset<Row>   data = sparkSession.createDataset(points, Encoders.STRING()).toDF("Accidents");
        writerCollections.accept(data);
        Dataset<Row> actual = sparkSession.read().csv(outputFolderPathStr).toDF("Accidents");
        assertThat(data.first().getString(0)).isEqualTo(actual.first().getString(0));
    }
    @After
    public void tearDown(){
        cleanup();
    }

    public void cleanup(){
        try{



            FileUtils.deleteDirectory(new File(outputFolderPathStr));

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


}
