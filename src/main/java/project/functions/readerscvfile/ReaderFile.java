package project.functions.readerscvfile;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.function.Supplier;

@Slf4j
@RequiredArgsConstructor
public class ReaderFile implements Supplier<Dataset<Row>>
{
    private final String inputPathStr;
    private final SparkSession sparSession;

    @Override
    public Dataset<Row> get() {
        log.info("reading file at inputPathStr={}", inputPathStr);
        try {
            Dataset<Row> lines = sparSession.read().option("delimiter", ",") .option( "header", "true").csv(inputPathStr);

            return lines;
        } catch (Exception exception) {
            log.error("failed to read file at inputPathStr={} due to ...", inputPathStr, exception);
        }
        return sparSession.emptyDataFrame();
    }
}

