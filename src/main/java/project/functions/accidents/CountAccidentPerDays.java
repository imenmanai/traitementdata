package project.functions.accidents;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import static org.apache.spark.sql.functions.count;

import java.util.function.Function;

public class CountAccidentPerDays  implements Function<Dataset<Row>,Dataset<Row>> {
    @Override
    public Dataset<Row> apply(Dataset<Row> datas) {
        Dataset<Row> dataset = datas.groupBy("DATE_ACCIDENT").agg(count(datas.col("DATE_ACCIDENT").as("Nombre d'accidents")));

        return dataset;
    }
}
