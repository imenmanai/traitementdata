package project.functions.accidents;

import lombok.RequiredArgsConstructor;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import project.classes.Configuration;

import java.util.*;
import java.util.function.Function;
//private Date DATE_ACCIDENT;
//private String HEURE;
//private  String SERVICE;
//private String POSTE;
//private String LIEU;
//private String ACCIDENT_TRAVAIL;
//private String ACCIDENT_TRAVAIL_TRAJET;
//private String MALADIE_PROFESSIONNELLE;
//private  Integer  NBR_JOURS_ARRET;
//private String AVEC_TIERS;
//private String ARRET;
//private String TEMOINS;
//private String NATURE_LESIONS;
//private String SIEGE_LESIONS;
//private String RISQUE_PROFESSIONNEL;
//private String CAUSE;
//private String OBJET;
//private String OBSERVATIONS;
@RequiredArgsConstructor

public class MapperFileSCV  implements Function<Dataset<Row>,Dataset<Row>> {

    @Override
    public Dataset<Row> apply(Dataset<Row> rows) {
      ///List<Row> list = List.of(rows.collectAsList());
              //new ArrayList<>(17);
         //    list = rows.collectAsList();
        //  Dataset<Row> test = rows.map((FlatMapFunction<Row>), Encoders.STRING());
Dataset<Row> test =rows.select("DATE_ACCIDENT","HEURE","SERVICE","POSTE","LIEU","ACCIDENT_TRAVAIL","ACCIDENT_TRAVAIL_TRAJET","MALADIE_PROFESSIONNELLE","AVEC/SANS ARRET","NBR_JOURS_ARRET","AVEC_TIERS","TEMOINS","ANCIENNETE*","NATURE_LESIONS","SIEGE_LESIONS","RISQUE_PROFESSIONNEL","CAUSE","OBJET","OBSERVATIONS"
).where(rows.col("DATE_ACCIDENT").startsWith("2018"))
   ;
        return test;
    }
    private FlatMapFunction<String, String> getWordFunction() {
        return line -> {
            Iterator<String> words = Arrays.stream(line.split(",")).iterator();
            return words;
        };}
}
