package project.functions.accidents;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.Arrays;
import java.util.Iterator;
import java.util.function.Function;
import static org.apache.spark.sql.functions.count;
import static org.apache.spark.sql.functions.countDistinct;

@Slf4j
@RequiredArgsConstructor
public class CountAccidentService implements Function<Dataset<Row>,Dataset<Row>> {

    @Override
    public Dataset<Row> apply(Dataset<Row> datas) {
      Dataset<Row> dataset = datas.groupBy("SERVICE").count().filter(datas.col("SERVICE").isNotNull());

        //Dataset<Row> dataset= datas.map()

        return dataset;
    }

}
