package project.services;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import project.classes.Configuration;
import project.functions.accidents.CountAccidentPerDays;
import project.functions.accidents.CountAccidentService;
import project.functions.accidents.MapperFileSCV;
import project.functions.readerscvfile.ReaderFile;
import project.functions.writerfile.ResultWriter;

public class MainProgramming {
    public static void main(String[] args) {

        Configuration conf = new Configuration();
        ReaderFile reader = new ReaderFile(conf.getInputPathStr(),conf.getSparkSession());
        Dataset<Row>  t =reader.get();
      //  t.select("NATURE_LESIONS").show();
        MapperFileSCV m = new MapperFileSCV();
       // CountAccidentService acc = new CountAccidentService();
        //acc.apply(t).show();
        CountAccidentPerDays c = new CountAccidentPerDays();
      // c.apply(m.apply(t)).show();
        ResultWriter r= new  ResultWriter(conf.getOutputPathStr());
        r.accept(m.apply(t));
        m.apply(t).show();

    }
}
