package project.classes;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.SparkConf;
import org.apache.spark.sql.SparkSession;
@Slf4j
@RequiredArgsConstructor
@Getter
public class Configuration {
    private final Config config;
    private final String inputPathStr;
    private final String outputPathStr;
    private final SparkConf sparkConf;
    private final SparkSession sparkSession;
    public Configuration()
    {
         config = ConfigFactory.load();
         inputPathStr = config.getString("3il.path.input");
         outputPathStr = config.getString("3il.path.output");
         sparkConf= new SparkConf().setMaster("local[2]").setAppName("WorkAccident");
         sparkSession= SparkSession.builder().config(sparkConf).getOrCreate();
    }
}
